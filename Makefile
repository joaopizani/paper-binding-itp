
DIRGUARD=@mkdir -p $(@D)

MAIN_NAME=main
REFERENCES_NAME=references
LATEXMKOPTS=-pdflatex="xelatex -shell-escape" -pdf -f -quiet
TEXT_PARTS_SRC=text

MAINTEX=$(MAIN_NAME).tex
MAINPDF=./$(MAIN_NAME).pdf
REFERENCES=$(REFERENCES_NAME).bib
TEXT_PARTS=section-introduction \
		   section-background \
		   section-generalization \
		   section-conclusion


CODE_AGDA_ROOT=code/agda
CODE_AGDA_SRC=$(CODE_AGDA_ROOT)/src
CODE_AGDA_GENTEX=$(CODE_AGDA_ROOT)/latex
CODE_AGDA_STY=agda.sty
CODE_AGDA_STDLIB=${HOME}/build/agda/lib/current/src

CODE_AGDA_MODULES=Basic


all: $(MAINPDF)

$(MAINPDF): \
	$(REFERENCES) $(MAINTEX) \
	$(TEXT_PARTS:%=$(TEXT_PARTS_SRC)/%.tex) \
	$(CODE_AGDA_STY) \
	$(CODE_AGDA_MODULES:%=$(CODE_AGDA_GENTEX)/%.tex)
	latexmk $(LATEXMKOPTS) $(MAINTEX)

$(CODE_AGDA_GENTEX)/%.tex: $(CODE_AGDA_SRC)/%.lagda
	$(DIRGUARD); agda --allow-unsolved-metas -i $(CODE_AGDA_SRC) --latex-dir=$(CODE_AGDA_GENTEX) --latex $<
	if [ -f $(subst /latex/,/patches/,$(@:.tex=.patch)) ]; then patch -d $(CODE_AGDA_ROOT) -p0 < $(subst /latex/,/patches/,$(@:.tex=.patch)); fi


clean:
	latexmk -c $(MAINTEX)

veryclean: clean
	rm -f $(CODE_AGDA_MODULES:%=$(CODE_AGDA_GENTEX)/%.tex)
	find $(CODE_AGDA_GENTEX) -type d -empty -delete

.PHONY: clean veryclean all

