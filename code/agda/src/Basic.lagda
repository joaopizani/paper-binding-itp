\begin{code}
module Basic where
\end{code}


%<*TyExp>
\begin{code}
data TyExp : Set where
    ℕₒ : TyExp
    Bₒ : TyExp
\end{code}
%</TyExp>

