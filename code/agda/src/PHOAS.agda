module PHOAS where

open import Data.String using (String) renaming (_++_ to _⧺_)
open import Data.Unit.Base using (⊤; tt)
open import Data.Nat.Base using (ℕ; zero; suc; _+_)
open import Data.Nat.Show using () renaming (show to showNat)
open import Data.Product using (_×_; _,_)
open import Relation.Binary.PropositionalEquality using (_≡_; refl; subst₂; cong)


infixl 6 _⨣_

data 𝔼 (α : Set) : Set where
  Num  : (n : ℕ) → 𝔼 α
  ⌈_⌉  : (v : α) → 𝔼 α
  Let  : (e : 𝔼 α) (f : α → 𝔼 α) → 𝔼 α
  
  _⨣_  : (e₁ e₂ : 𝔼 α) → 𝔼 α

fromNat : ∀ {α} (n : ℕ) → 𝔼 α
fromNat = Num

{-# BUILTIN FROMNAT fromNat #-}
  

𝔼′ : Set₁
𝔼′ = ∀ {α} → 𝔼 α


infixr 5 Let
syntax Let e (λ x → b) = letₑ x ← e inₑ b


data _⊢_≡ₑ_ {α β : Set} : (α → β → Set) → 𝔼 α → 𝔼 β → Set₁ where
  Num₌  : ∀ {P x}             → P ⊢ Num x ≡ₑ Num x
  ⌈_⌉₌  : ∀ {P x y}           → P x y → P ⊢ ⌈ x ⌉ ≡ₑ ⌈ y ⌉
  Let₌  : ∀ {P b₁ b₂ e₁ e₂}  → (P ⊢ b₁ ≡ₑ b₂) → (∀ {x₁ x₂} → (P x₁ x₂ → P ⊢ e₁ x₁ ≡ₑ e₂ x₂)) → P ⊢ (Let b₁ e₁) ≡ₑ (Let b₂ e₂)

  _⨣₌_   : ∀ {x₁ y₁ x₂ y₂ Γ} → Γ ⊢ x₁ ≡ₑ x₂ → Γ ⊢ y₁ ≡ₑ y₂ → Γ ⊢ (x₁ ⨣ y₁) ≡ₑ (x₂ ⨣ y₂)

-- Parametricity meta-language property
postulate ≡ₑ𝔼′ : ∀ {α β P} {e : 𝔼′} → P ⊢ e {α} ≡ₑ e {β}




⟦_⟧ : 𝔼 ℕ → ℕ
⟦ Num n ⟧    = n
⟦ ⌈ x ⌉ ⟧    = x
⟦ Let x e ⟧  = ⟦ e (⟦ x ⟧) ⟧
⟦ e₁ ⨣ e₂ ⟧  = ⟦ e₁ ⟧ + ⟦ e₂ ⟧

⟦_⟧′ : 𝔼′ → ℕ
⟦ e ⟧′ = ⟦ e {ℕ} ⟧


#vars : 𝔼 ⊤ → ℕ
#vars (Num n)    = zero
#vars (⌈ x ⌉)    = zero
#vars (e₁ ⨣ e₂)  = #vars e₁ + #vars e₂
#vars (Let e f)  = (suc zero) + (#vars e + #vars (f tt))


tree : ∀ {α} (depth : ℕ) → 𝔼 α
tree zero     = Num zero
tree (suc n)  =  letₑ x ← tree n
                 inₑ ⌈ x ⌉ ⨣ ⌈ x ⌉


show𝔼′ : 𝔼′ → String
show𝔼′ e = go e zero
  where  go : (e : 𝔼 String) (v# : ℕ) → String
         go (Num n)    _ = showNat n
         go (⌈ x ⌉)    _ = x
         go (e₁ ⨣ e₂)  n = "(" ⧺  go e₁ n  ⧺  " + "  ⧺  go e₂ n  ⧺ ")"
         go (Let e f)  n =  let  v = "v" ⧺ showNat n
                            in   "let " ⧺ v ⧺ " = " ⧺  go e (suc n)  ⧺
                                 " in " ⧺  go (f v) (suc n)  ⧺ ")"


inline : ∀ {α} → 𝔼 (𝔼 α) → 𝔼 α
inline (Num n)    = Num n
inline ⌈ x ⌉      = x
inline (Let x e)  = inline (e (inline x))
inline (e₁ ⨣ e₂)  = (inline e₁) ⨣ (inline e₂)


constantFolding : 𝔼 ℕ → 𝔼 ℕ
constantFolding (Num n)    = Num n
constantFolding (⌈ x ⌉)    = ⌈ x ⌉
constantFolding (e₁ ⨣ e₂)  = Num (⟦ e₁ ⟧ + ⟦ e₂ ⟧)
constantFolding (Let e f)  = Let (constantFolding e) (λ x → constantFolding (f x))



data _≃_ : ℕ → 𝔼 ℕ → Set where
  Eq : ∀ {n : ℕ} {e : 𝔼 ℕ} → n ≡ ⟦ e ⟧ → n ≃ e


constantFoldingEquality' : ∀ {e} → ⟦ e ⟧ ≡ ⟦ constantFolding e ⟧
constantFoldingEquality' {Num n}    = refl
constantFoldingEquality' {⌈ x ⌉}    = refl
constantFoldingEquality' {e₁ ⨣ e₂}  = refl
constantFoldingEquality' {Let e f} with (cong f (constantFoldingEquality' {e}))
... | q rewrite q = constantFoldingEquality' {f ⟦ constantFolding e ⟧}

constantFoldingEquality : ∀ {e : 𝔼′} → ⟦ e ⟧ ≡ ⟦ constantFolding e ⟧
constantFoldingEquality {e} = constantFoldingEquality' {e {ℕ}}


+-injective : ∀ {x₁ x₂ y₁ y₂} → x₁ ≡ x₂ → y₁ ≡ y₂ → (x₁ + y₁) ≡ (x₂ + y₂)
+-injective x≡ y≡ rewrite x≡ | y≡ = refl

--inlineEquality′ : ∀ {e₁ : 𝔼 ℕ} {e₂ : 𝔼 (𝔼 ℕ)} → _≃_ ⊢ e₁ ≡ₑ e₂ → ⟦ e₁ ⟧ ≡ ⟦ inline e₂ ⟧
--inlineEquality′ Num₌        = refl
--inlineEquality′ ⌈ x≋y ⌉₌    = {!!}
--inlineEquality′ (x≋ ⨣₌ y≋)  = +-injective (inlineEquality′ x≋) (inlineEquality′ y≋)
--inlineEquality′ {Let b₁ e₁} {Let b₂ e₂} (Let₌ b≋ e≋) with e≋ {⟦ b₁ ⟧} {inline b₂}
--inlineEquality′ {Let b₁ e₁} {Let b₂ e₂} (Let₌ b≋ e≋) | app≋ = inlineEquality′ app≋
--
--inlineEquality : ∀ {e : 𝔼′} → ⟦ e ⟧′ ≡ ⟦ inline e ⟧′
--inlineEquality {e} = inlineEquality′ (≡ₑ𝔼′ {ℕ} {𝔼 ℕ} {e})



test₁ : 𝔼 ℕ
test₁ = 32

test₂ : 𝔼 ℕ
test₂ =       letₑ x ← 32
         inₑ  letₑ y ← 42
         inₑ  ⌈ x ⌉ ⨣ ⌈ y ⌉
